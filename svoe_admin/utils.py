import os

from django.core.exceptions import ImproperlyConfigured


def get_env_variable_value(env_variable_name):
    '''Return env variable value or raise error if
    the variable is not defined

    '''
    try:
        return os.environ[env_variable_name]
    except KeyError:
        error_message = f'Set the {env_variable_name} environment variable'

        raise ImproperlyConfigured(error_message)


def add_class_to_img_table(data, field_name):
    counter = 0
    new_text = data[field_name]
    while True:
        start = data[field_name][counter:].find('<table')
        if start == -1:
            break
        start += counter + 1
        end_tag = data[field_name][start:].find('</table')
        if '<img' in data[field_name][start:start + end_tag]:
            end = data[field_name][start:].find('>')
            if end == 0:
                end = data[field_name][start+1:].find('>')
            table_string = data[field_name][start:start + end] + ' class="img-table>"'
            new_text = data[field_name][:start] + table_string + data[field_name][start + end + 1:]
            counter = start + end
        else:
            counter = start
    return new_text
