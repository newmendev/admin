import os
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
from svoe_admin.utils import get_env_variable_value

BASE_DIR = Path(__file__).resolve().parent.parent

SECRET_KEY = get_env_variable_value('SECRET_KEY')

debug_flag = get_env_variable_value('DEBUG')

if debug_flag.lower() == 'false':
    DEBUG = False
else:
    DEBUG = True

ALLOWED_HOSTS = [
    h.strip() for h in get_env_variable_value('ALLOWED_HOSTS').split(',')
]

X_FRAME_OPTIONS = 'ALLOWALL'

# Application definition

INSTALLED_APPS = [
    'ckeditor',
    'ckeditor_uploader',
    'grappelli',
    'django_summernote',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'main_service'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
]

ROOT_URLCONF = 'svoe_admin.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates']
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'svoe_admin.wsgi.application'

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': get_env_variable_value('DB_NAME'),
        'USER': get_env_variable_value('DB_USER'),
        'PASSWORD': get_env_variable_value('DB_PASSWORD'),
        'HOST': get_env_variable_value('DB_HOST'),
        'PORT': int(get_env_variable_value('DB_PORT'))
    }
}

# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

STATIC_URL = '/static/'

STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]

STATIC_ROOT = os.path.join(BASE_DIR, 'data_static')

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'


CKEDITOR_CONFIGS = {
    'default': {
        'stylesSet': [
            # Block style
            {
                "name": 'Заголовок Цели',
                "element": 'div',
                "attributes": {
                    'class': 'target_title',
                    'style': 'color: white;'
                     'background-color: rgba(19, 165, 56, 1); padding: 70px 70px 18px 70px;'
                     'border-top-left-radius: 5px;border-top-right-radius: 5px;font-size: 35px;'
                     'font-family: Montserrat; line-height: 50px;'
                },
            },
            {
                "name": 'Заголовок Цели Жирный',
                "element": 'div',
                "attributes": {
                    'class': 'target_title',
                    'style': 'color: white;'
                             'background-color: rgba(19, 165, 56, 1); padding: 70px 70px 18px 70px;'
                             'border-top-left-radius: 5px;border-top-right-radius: 5px;font-size: 35px; font-weight: bold;'
                             'font-family: Montserrat; line-height: 50px;'
                },
            },
            {
                "name": 'Контент',
                "element": 'div',
                "attributes": {
                    'class': 'target_content',
                    'style': 'color: white;background-color: rgba(19, 165, 56, 1); padding: 70px 70px 18px 70px;'
                             'border-bottom-left-radius: 5px; font-size: 23px;'
                             'border-bottom-right-radius: 5px; font-family: Montserrat; line-height: 36px;'
                },
            },
            {
                "name": 'Контент скругленный',
                "element": 'div',
                "attributes": {
                    'class': 'target_content_round',
                    'style': 'color: white;background-color: rgba(19, 165, 56, 1); padding: 70px 70px 70px 70px;'
                             'border-radius: 20px; font-size: 23px;'
                             'font-family: Montserrat; line-height: 36px;'
                },
            },
            # Element style
            {
                "name": 'Текст',
                "element": 'span',
                "attributes": {
                    'class': 'text_default',
                    'style': 'font-size: 18px; line-height: 33px; letter-spacing: 0em; text-align: '
                    'left; font-style: normal; font-family: Montserrat;'
                },
            },
            {
                "name": 'Заголовок',
                "element": 'span',
                "attributes": {
                    'class': 'text_title',
                    'style': 'font-size: 23px; line-height: 33px; letter-spacing: 0em; '
                    'text-align: left; font-style: normal; font-family: Montserrat; font-weight: 400'
                },
             },
            {
                "name": 'Заголовок Жирный',
                "element": 'h3',
                "attributes": {
                    'class': 'text_title_bold',
                    'style': 'font-size: 23px; line-height: 33px; letter-spacing: 0em;'
                        'text-align: left; font-weight: bold; font-family: Montserrat;'
                },
             },
            {
                "name": '"',
                "element": 'span',
                "attributes": {
                    'class': 'quote_symbol',
                    'style': 'color: rgba(19, 165, 56, 1);font-size: 57px; font-family: Montserrat;'
                },
             },
            {
                "name": 'Цифра',
                "element": 'span',
                "attributes": {
                    'class': 'number_list',
                    'style': 'color: rgba(19, 165, 56, 1);'
                    'font-size: 18px; font-family: Montserrat;'
                },
            },
            {
                "name": 'Описание картинки',
                "element": 'span',
                "attributes": {
                    'class': 'citata13',
                    'style': 'font-size: 15px; font-family: Montserrat;'
                },
            },
        ],
        'toolbar_Full': [
            ['Styles', 'Underline',  'Undo', 'Redo'],
            ['Link', 'Unlink'],
            ['Image', 'Table', 'HorizontalRule'],
            ['TextColor', 'BGColor'],
            ['Smiley','sourcearea', 'SpecialChar'],
            ['Source'],
        ],
        'height': '500px',
        'width': '1000px'
    },
}
CKEDITOR_RESTRICT_BY_DATE = False
CKEDITOR_BASEPATH = "/static/ckeditor/ckeditor/"
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_UPLOAD_SLUGIFY_FILENAME = False