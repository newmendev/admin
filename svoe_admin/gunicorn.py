# gunicorn -c=svoe_admin_service/gunicorn.py cvoe_admin_service.wsgi:application

import os
reload = bool(eval(os.getenv("GUNICORN_RELOAD", "False").title()))
threads = int(os.getenv("GUNICORN_THREADS") or 5)
max_requests = int(os.getenv("GUNICORN_MAX_REQUESTS") or 25000)
timeout = int(os.getenv('GUNICORN_TIMEOUT') or 1500)
