import math
from io import BytesIO
from PIL import Image
from django.core.files import File
from django.template.defaultfilters import slugify as django_slugify


def compress(image):
    im = Image.open(image)
    im_io = BytesIO()
    if im.width > 1500:
        pr = math.floor(im.width / 1500)
        im.thumbnail((math.ceil(im.width / pr), math.ceil(im.height / pr)))
        im.save(im_io, 'JPEG', quality=80)
    else:
        im.save(im_io, 'JPEG', quality=80)
    new_image = File(im_io, name=image.name)
    return new_image


alphabet = {'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh', 'з': 'z', 'и': 'i',
            'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't',
            'у': 'u', 'ф': 'f', 'х': 'kh', 'ц': 'ts', 'ч': 'ch', 'ш': 'sh', 'щ': 'shch', 'ы': 'i', 'э': 'e', 'ю': 'yu',
            'я': 'ya'}


def slugify(s):
    """
    Overriding django slugify that allows to use russian words as well.
    """
    return django_slugify(''.join(alphabet.get(w, w) for w in s.lower()))