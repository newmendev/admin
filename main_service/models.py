import datetime

from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField

from main_service.utils import compress, slugify


class BaseInfo(models.Model):
    title_ru = models.CharField(max_length=400,
                                verbose_name="Заголовок (русский)")
    title_en = models.CharField(max_length=400,
                                verbose_name="Заголовок (английский)")
    address_ru = models.CharField(max_length=200,
                                  verbose_name="Адрес (русский)")
    address_en = models.CharField(max_length=200,
                                  verbose_name="Адрес (английский)")
    youtube = models.URLField(verbose_name="Ссылка на youtube")
    instagram = models.URLField(verbose_name="Ссылка на инстаграм")
    about_ru = models.TextField(verbose_name="О компании (русский)")
    about_en = models.TextField(verbose_name="О компании (английский)")
    about_additional_ru = models.TextField(default='',
                                           verbose_name="Дополнительно о компании (русский)")
    about_additional_en = models.TextField(default='',
                                           verbose_name="Дополнительно о компании (английский)")
    phone = models.CharField(max_length=30, default='', verbose_name="Телефон")
    email = models.CharField(max_length=200, default='', verbose_name='Email')
    video_url = models.TextField(max_length=50, default='',
                                 verbose_name='Ссылка на видео')
    show_video = models.BooleanField(default=True,
                                     verbose_name='Показывать видео')

    def __str__(self):
        return self.title_ru

    class Meta:
        managed = False
        db_table = 'base_info'
        verbose_name = "Базовую информация"
        verbose_name_plural = "Базовая информация"


class Team(models.Model):
    photo = models.ImageField(upload_to='static/images/',
                              default='static/images/user_default.svg',
                              verbose_name="Фото")
    name_ru = models.CharField(max_length=255, verbose_name="Имя (русский)")
    name_en = models.CharField(max_length=255, verbose_name="Имя (английский)")
    position_ru = models.CharField(max_length=255,
                                   verbose_name="Должность (русский)")
    position_en = models.CharField(max_length=255,
                                   verbose_name="Должность (английский)")
    details_ru = models.TextField(verbose_name="Детали (русский)")
    details_en = models.TextField(verbose_name="Детали (английский)")
    is_trustee = models.BooleanField(default=False,
                                     verbose_name="Входит в попечительский совет")
    priority = models.IntegerField(default=1)

    def __str__(self):
        return self.name_ru

    class Meta:
        managed = False
        db_table = 'team'
        verbose_name = "Участника команды"
        verbose_name_plural = "Команда"


class Competence(models.Model):
    content_ru = models.CharField(max_length=255,
                                  verbose_name="Текст (русский)")
    content_en = models.CharField(max_length=255,
                                  verbose_name="Текст (английский)")
    priority = models.IntegerField(unique=True, verbose_name='Приоритет')

    def __str__(self):
        return self.content_ru

    class Meta:
        managed = False
        db_table = 'competence'
        verbose_name = "Компетенцию"
        verbose_name_plural = "Компетенции"


class Partners(models.Model):
    img = models.ImageField(upload_to='static/images/',
                            verbose_name="Изображение")
    text = models.CharField(max_length=255, verbose_name="Текст", default='',
                            blank=True)
    url = models.URLField(verbose_name="Ссылка")
    priority = models.IntegerField(unique=True, default=None, null=True,
                                   verbose_name='Приоритет')

    class Meta:
        managed = False
        db_table = 'partners'
        verbose_name = "Партнера"
        verbose_name_plural = "Партнеры"

    def __str__(self):
        return self.url


class Documents(models.Model):
    document = models.FileField(upload_to='static/documents/',
                                verbose_name='Документ')
    text = models.CharField(max_length=50, verbose_name='Текст', default='',
                            blank=True)
    priority = models.IntegerField(unique=True, verbose_name='Приоритет')
    is_show = models.BooleanField(default=False, verbose_name='Показать')

    class Meta:
        managed = False
        db_table = 'documents'
        verbose_name = 'Документ'
        verbose_name_plural = 'Документы'


class News(models.Model):
    title_ru = models.CharField(max_length=255,
                                verbose_name="Заголовок (русский)")
    title_en = models.CharField(max_length=255, blank=True, default='',
                                verbose_name="Заголовок (английский)")
    text_ru = RichTextUploadingField(default='',
                                     verbose_name="Текст (русский)")
    text_en = RichTextUploadingField(default='',
                                     verbose_name="Текст (английский)")
    pic = models.ImageField(upload_to='static/images/',
                            default='static/images/news_default.svg',
                            verbose_name="Картинка")
    partner = models.ImageField(upload_to='static/images/', default=None,
                                blank=True, verbose_name="Партнер")
    data = models.DateField(verbose_name="Дата")
    news_tags = models.TextField(default='', blank=True, verbose_name="Теги")
    is_hidden = models.BooleanField(default=False,
                                    verbose_name="Скрыть новость")
    is_main = models.BooleanField(default=False, verbose_name="На главной")
    title = models.TextField(default='', verbose_name='SEO Title', blank=True)
    description = models.TextField(default='', verbose_name='SEO Description',
                                   blank=True)
    keyword = models.TextField(default='', verbose_name='SEO Keyword',
                               blank=True)
    url_str = models.CharField(max_length=50, default='',
                               verbose_name='URL новости', unique=True)

    def __str__(self):
        return self.title_ru

    class Meta:
        managed = False
        db_table = 'news'
        verbose_name = "Новость"
        verbose_name_plural = "Новости"


class ProjectTypes(models.Model):
    name_ru = models.CharField(max_length=255,
                               verbose_name="Название (русский)")
    name_en = models.CharField(max_length=255, default='',
                               verbose_name="Название (английский)")
    priority = models.IntegerField(unique=True, default=None, null=True,
                                   verbose_name="Приоритет")

    def __str__(self):
        return self.name_ru

    class Meta:
        managed = False
        db_table = 'project_types'
        verbose_name = "Тип проекта"
        verbose_name_plural = "Типы проектов"


class MediaTypes(models.Model):
    name_ru = models.CharField(max_length=255,
                               verbose_name="Название (русский)")
    name_en = models.CharField(max_length=255, default='',
                               verbose_name="Название (английский)")
    priority = models.IntegerField(unique=True, default=None, null=True,
                                   verbose_name="Приоритет")

    def __str__(self):
        return self.name_ru

    class Meta:
        managed = False
        db_table = 'media_types'
        verbose_name = "Типы медиа"
        verbose_name_plural = "Тип медиа"


class Videos(models.Model):
    project_type = models.ForeignKey(MediaTypes, on_delete=models.DO_NOTHING,
                                     verbose_name="Тип проекта", default=None)
    title_ru = models.CharField(max_length=255,
                                verbose_name="Заголовок (русский)")
    title_en = models.CharField(max_length=255, default='',
                                verbose_name="Заголовок (английский)")
    url = models.CharField(max_length=200, verbose_name="Ссылка")
    preview = models.ImageField(upload_to='static/images/',
                                verbose_name="Превью")
    date = models.DateField(verbose_name="Дата")
    big_block = models.BooleanField(default=False, verbose_name="Большой блок")
    title = models.TextField(default='', verbose_name='SEO Title', blank=True)
    description = models.TextField(default='', verbose_name='SEO Description',
                                   blank=True)
    url_str = models.CharField(max_length=50, default='',
                               verbose_name='URL видео', unique=False)

    def __str__(self):
        return self.title_ru

    class Meta:
        managed = False
        db_table = 'videos'
        verbose_name = "Видео"
        verbose_name_plural = "Видео"


class Projects(models.Model):
    project_type = models.ForeignKey(ProjectTypes, on_delete=models.DO_NOTHING,
                                     verbose_name="Тип проекта")
    title_ru = models.CharField(max_length=255,
                                verbose_name="Заголовок (русский)")
    title_en = models.CharField(max_length=255, blank=True, default='',
                                verbose_name="Заголовок (английский)")
    text_ru = RichTextUploadingField(default='',
                                     verbose_name="Текст (русский)")
    text_en = RichTextUploadingField(default='',
                                     verbose_name="Текст (английский)")
    pic = models.ImageField(upload_to='static/images/',
                            default='static/images/project_default.svg',
                            verbose_name="Картинка")
    big_block = models.BooleanField(default=False, verbose_name="Большой блок")
    content = RichTextUploadingField(default='', null=True)
    data = models.DateField(default=datetime.date.today, verbose_name='Дата')
    title = models.TextField(default='', verbose_name='SEO Title', blank=True)
    description = models.TextField(default='', verbose_name='SEO Description',
                                   blank=True)
    keyword = models.TextField(default='', verbose_name='SEO Keyword',
                               blank=True)
    url_str = models.CharField(max_length=50, default='',
                               verbose_name='URL проекта', unique=True)

    def __str__(self):
        return self.title_ru

    class Meta:
        managed = False
        db_table = 'projects'
        verbose_name = "Проект"
        verbose_name_plural = "Проекты"


class FarmTypes(models.Model):
    name_ru = models.CharField(max_length=255, verbose_name='Имя на русском')
    name_en = models.CharField(max_length=255, default='',
                               verbose_name='Имя на английском')

    def __str__(self):
        return self.name_ru

    class Meta:
        managed = True
        db_table = 'farm_types'
        verbose_name = "Занятие фермы"
        verbose_name_plural = "Занятия фермы"


class Releases(models.Model):
    title_ru = models.CharField(max_length=255,
                                verbose_name='Заголовок на русском')
    title_en = models.CharField(max_length=255, blank=True, default='',
                                verbose_name='Заголовок на английском')
    video_url = models.URLField(blank=True, default='',
                                verbose_name='Ссылка на видео')
    video_preview_pic = models.ImageField(upload_to='static/images/',
                                          verbose_name='Превью')
    release_types = models.ManyToManyField(FarmTypes, blank=True,
                                           verbose_name='Занятия фермы')

    # Основной текст
    text_ru = RichTextUploadingField(verbose_name='Текст на русском')
    text_en = RichTextUploadingField(default='',
                                     verbose_name='Текст на английском')

    # Личная история
    history_hero_pic = models.ImageField(upload_to='static/images/',
                                         verbose_name='Фото героя')
    history_hero_name_ru = models.CharField(max_length=255, blank=True,
                                            verbose_name='Имя героя на русском',
                                            default='')
    history_hero_name_en = models.CharField(max_length=255, blank=True,
                                            verbose_name='Имя героя на английском',
                                            default='')
    history_hero_post_ru = models.CharField(max_length=255, blank=True,
                                            verbose_name='Должность героя на русском',
                                            default='')
    history_hero_post_en = models.CharField(max_length=255, blank=True,
                                            verbose_name='Должность нероя на английском',
                                            default='')
    history_hero_text_ru = RichTextUploadingField(default='',
                                                  verbose_name='Текст героя на русском')
    history_hero_text_en = RichTextUploadingField(default='', blank=True,
                                                  verbose_name='Текст героя на английском')

    # Геолокация и контакты
    map_lat = models.CharField(max_length=255, verbose_name='Долгота')
    map_lng = models.CharField(max_length=255, verbose_name='Широта')
    address_name = models.CharField(max_length=255, default='',
                                    verbose_name='Имя адреса')
    address_url = models.CharField(max_length=255, blank=True, default='',
                                   verbose_name='Ссылка на адрес (url)(клик по адресу)')
    contact_text_ru = RichTextUploadingField(
        verbose_name='Блок контактов на русском')
    contact_text_en = RichTextUploadingField(default='', blank=True,
                                             verbose_name='Блок контактов на английском')
    farm_site_url = models.URLField(blank=True, default='',
                                    verbose_name='Ссылка на сайт')
    farm_instagram_url = models.URLField(blank=True, default='',
                                         verbose_name='Ссылка на instagram')
    farm_vk_url = models.URLField(blank=True, default='',
                                  verbose_name='Ссылка на VK')
    farm_fb_url = models.URLField(blank=True, default='',
                                  verbose_name='Ссылка на FB')

    # Базовые поля
    date = models.DateField(default=datetime.date.today,
                            verbose_name='Дата выпуска')
    url_str = models.CharField(max_length=50, default='', blank=True,
                               verbose_name='URL выпуска')

    # Значок
    badge_enabled = models.BooleanField(default=False, blank=False,
                                        verbose_name='Значок')

    def __str__(self):
        return self.title_ru

    def save(self, *args, **kwargs):
        if not self.url_str:
            self.url_str = slugify(self.title_ru)
        super(Releases, self).save(*args, **kwargs)

    class Meta:
        managed = True
        db_table = 'releases'
        verbose_name = "Выпуск"
        verbose_name_plural = "Выпуски"




class Product(models.Model):
    release = models.ForeignKey(Releases, on_delete=models.DO_NOTHING,
                                verbose_name='Выпуск',
                                related_name='popular_products')
    name_ru = models.CharField(max_length=255,
                               verbose_name='Название на русском')
    name_en = models.CharField(max_length=255, default='',
                               verbose_name='Название на английском')
    url_to_shop = models.CharField(max_length=255, default='',
                                   verbose_name='Ссылка в магазин (url)')
    pic = models.ImageField(upload_to='static/images/',
                            verbose_name='Фото')

    def __str__(self):
        return self.name_ru

    class Meta:
        managed = True
        db_table = 'farm_products'
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"


class ReleaseImage(models.Model):
    release = models.ForeignKey(Releases, on_delete=models.DO_NOTHING,
                                verbose_name='Выпуск', related_name='images')
    pic = models.ImageField(upload_to='static/images/', verbose_name='Фото')

    class Meta:
        managed = True
        db_table = 'farm_images'
        verbose_name = "Фото выпуска"
        verbose_name_plural = "Фото выпуска"
