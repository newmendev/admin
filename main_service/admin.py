from django.contrib import admin, messages
from django.contrib.admin import TabularInline
from django.utils.translation import ugettext_lazy as _

from svoe_admin.utils import get_env_variable_value, add_class_to_img_table
from .models import BaseInfo, Team, News, ProjectTypes, Videos, Projects, \
    MediaTypes, Competence, Partners, Documents
from .models import FarmTypes, Releases, Product, ReleaseImage
from django.contrib.auth.models import Group
from django_summernote.models import Attachment
import math
from django.utils.safestring import mark_safe

from .utils import compress


class BaseInfoAdmin(admin.ModelAdmin):
    fields = [
        'title_ru', 'title_en', 'address_ru', 'phone', 'address_en',
        'youtube', 'instagram', 'about_ru', 'about_en', 'about_additional_ru',
        'about_additional_en', 'email', 'video_url', 'show_video'
    ]

    class Meta:
        verbose_name_plural = _("Базовая информация")

    def has_add_permission(self, request):
        if BaseInfo.objects.all().count():
            return False
        else:
            return True

    def has_change_permission(self, request, obj=None):
        return True

    def has_delete_permission(self, request, obj=None):
        return False


class TeamAdmin(admin.ModelAdmin):
    fields = ['photo', 'name_ru', 'name_en', 'position_ru', 'position_en',
              'details_ru', 'details_en', 'is_trustee', 'priority']
    list_display = ('name_ru', 'is_trustee', 'priority')

    class Meta:
        verbose_name_plural = _("Команда")


class NewsAdmin(admin.ModelAdmin):
    fields = [
        'title_ru', 'title_en', 'text_ru', 'text_en', 'pic',
        'partner', 'data', 'news_tags', 'is_hidden', 'is_main',
        'title', 'description', 'keyword', 'url_str'
    ]
    summernote_fields = ('text_ru', 'text_en',)
    list_display = ('title_ru', 'data', 'is_hidden', 'is_main', 'url_str')

    def save_model(self, request, obj, form, change):
        if 'pic' in form.changed_data:
            new_image = compress(obj.pic)
            obj.pic = new_image

        if 'text_ru' in form.changed_data:
            _mutable = form.data._mutable
            form.data._mutable = True
            form.data['text_ru'] = add_class_to_img_table(form.data, 'text_ru')
            form.data._mutable = _mutable
        if 'text_en' in form.changed_data:
            _mutable = form.data._mutable
            form.data._mutable = True
            form.data['text_ru'] = add_class_to_img_table(form.data, 'text_en')
            form.data._mutable = _mutable
        super(NewsAdmin, self).save_model(request, obj, form, change)


class ProjectTypesAdmin(admin.ModelAdmin):
    fields = ['name_ru', 'name_en', 'priority']
    list_display = ('name_ru', 'priority')


class MediaTypesAdmin(admin.ModelAdmin):
    fields = ['name_ru', 'name_en', 'priority']
    list_display = ('name_ru', 'priority')


class VideosAdmin(admin.ModelAdmin):
    fields = ['project_type', 'title_ru', 'title_en', 'url', 'preview', 'date']
    list_display = ('title_ru', 'date')

    def save_model(self, request, obj, form, change):
        if 'preview' in form.changed_data:
            new_image = compress(obj.preview)
            obj.preview = new_image

        super(VideosAdmin, self).save_model(request, obj, form, change)


class ProjectsAdmin(admin.ModelAdmin):
    end_block_index = []
    projects = list(Projects.objects.order_by('-id').all())
    count = len(projects)
    while 1:
        if end_block_index:
            idx = end_block_index[-1] - 7
            if idx < 0:
                break
            else:
                end_block_index.append(idx)
        else:
            idx = count - 7
            if idx < 0:
                break
            else:
                end_block_index.append(count - 7)

    def get_form(self, request, obj=None, **kwargs):
        form = super(ProjectsAdmin, self).get_form(request, obj, **kwargs)
        try:
            del form.base_fields['content']
        except:
            pass
        if obj is None:
            return form

        if obj.big_block:
            return form

        idx = 0
        block_count = 0

        for i in range(math.ceil(self.count / 8)):
            is_block = False
            if math.ceil(self.count / 8) - 1 == i:
                for p in self.projects[i:]:
                    if p.id == obj.id:
                        is_block = True
                        idx = i
                        block_count = len(self.projects[i:])
                        break
            else:
                if i == 0:
                    for p in self.projects[i:i + 7]:
                        if p.id == obj.id:
                            is_block = True
                            idx = i
                            block_count = len(self.projects[i:i + 7])
                            break
                else:
                    for p in self.projects[i * 8:(i * 8) + 7]:
                        if p.id == obj.id:
                            is_block = True
                            idx = i
                            block_count = len(self.projects[i:i + 7])
                            break
            if is_block:
                break

        offset = idx * 8
        last_projects = Projects.objects.all().order_by('-id')[
                        offset:offset + 8]
        count = 0
        for i in last_projects:
            if i.big_block:
                count += 1
        if count >= 1 and block_count >= 3:
            form.base_fields['big_block'].disabled = True
        return form

    def film_status(self, obj):
        if obj.id in self.end_block_index:
            return mark_safe(
                '<div style="width:20px; height:20px; border-radius: 50%; background-color:silver;"></div>')
        return ''

    film_status.allow_tags = True

    def get_list_display(self, request):
        base_list_display = super(ProjectsAdmin, self).get_list_display(
            request)
        base_list_display = list(base_list_display)
        base_list_display.append('big_block')
        base_list_display.append('film_status')
        base_list_display.append('data')
        base_list_display.append('url_str')
        base_list_display.insert(0, 'id')
        return base_list_display

    def save_model(self, request, obj, form, change):
        if 'pic' in form.changed_data:
            new_image = compress(obj.pic)
            obj.pic = new_image

        if 'text_ru' in form.changed_data:
            _mutable = form.data._mutable
            form.data._mutable = True
            form.data['text_ru'] = add_class_to_img_table(form.data, 'text_ru')
            form.data._mutable = _mutable
        if 'text_en' in form.changed_data:
            _mutable = form.data._mutable
            form.data._mutable = True
            form.data['text_ru'] = add_class_to_img_table(form.data, 'text_en')
            form.data._mutable = _mutable

        if 'big_block' in form.changed_data and not form.cleaned_data[
            'big_block']:
            messages.add_message(request, messages.INFO,
                                 'Возможны различия разметки, пожалуйста выберите 2 больший блока на каждые 16 постов')
        super(ProjectsAdmin, self).save_model(request, obj, form, change)


class CompetenceAdmin(admin.ModelAdmin):
    fields = ['content_ru', 'content_en', 'priority']
    list_display = ('content_ru', 'priority')

    class Meta:
        verbose_name_plural = _("Компетенции")

    def has_add_permission(self, request):
        if Competence.objects.all().count() >= 6:
            return False
        else:
            return True


class PartnersAdmin(admin.ModelAdmin):
    fields = ['img', 'text', 'url', 'priority']
    list_display = ('url', 'text', 'priority')

    class Meta:
        verbose_name_plural = _("Партнеры")


class DocumentsAdmin(admin.ModelAdmin):
    fields = ['document', 'text', 'priority', 'is_show']
    list_display = ('text', 'priority', 'is_show')

    class Meta:
        verbose_name_plural = _("Документы")


class UsersAdmin(admin.ModelAdmin):
    class Meta:
        verbose_name_plural = _("Команда")


class GroupsAdmin(admin.ModelAdmin):
    class Meta:
        verbose_name_plural = _("Команда")


class FarmTypesAdmin(admin.ModelAdmin):
    fields = ['name_ru', 'name_en']


class ProductInline(TabularInline):
    model = Product


class ReleaseImageInline(TabularInline):
    model = ReleaseImage


class ReleasesAdmin(admin.ModelAdmin):
    inlines = [
        ProductInline,
        ReleaseImageInline
    ]
    summernote_fields = ('text_ru', 'text_en',)
    list_display = ['title_ru', 'url_str']

    def save_model(self, request, obj, form, change):
        if 'text_ru' in form.changed_data:
            _mutable = form.data._mutable
            form.data._mutable = True
            form.data['text_ru'] = add_class_to_img_table(form.data, 'text_ru')
            form.data._mutable = _mutable
        if 'text_en' in form.changed_data:
            _mutable = form.data._mutable
            form.data._mutable = True
            form.data['text_ru'] = add_class_to_img_table(form.data, 'text_en')
            form.data._mutable = _mutable
        if 'history_hero_text_ru' in form.changed_data:
            _mutable = form.data._mutable
            form.data._mutable = True
            form.data['history_hero_text_ru'] = add_class_to_img_table(
                form.data, 'history_hero_text_ru')
            form.data._mutable = _mutable
        if 'history_hero_text_en' in form.changed_data:
            _mutable = form.data._mutable
            form.data._mutable = True
            form.data['history_hero_text_en'] = add_class_to_img_table(
                form.data, 'history_hero_text_en')
            form.data._mutable = _mutable
        if 'contact_text_ru' in form.changed_data:
            _mutable = form.data._mutable
            form.data._mutable = True
            form.data['contact_text_ru'] = add_class_to_img_table(form.data,
                                                                  'contact_text_ru')
            form.data._mutable = _mutable
        if 'contact_text_en' in form.changed_data:
            _mutable = form.data._mutable
            form.data._mutable = True
            form.data['contact_text_en'] = add_class_to_img_table(form.data,
                                                                  'contact_text_en')
            form.data._mutable = _mutable

        super(ReleasesAdmin, self).save_model(request, obj, form, change)


admin.site.register(FarmTypes, FarmTypesAdmin)
admin.site.register(Releases, ReleasesAdmin)

admin.site.site_header = 'Фонд СВОЕ'
admin.site.site_title = 'Фонд СВОЕ'
admin.site.enable_nav_sidebar = True
admin.site.site_url = get_env_variable_value('ADMIN_SITE_URL')

admin.site.register(ProjectTypes, ProjectTypesAdmin)
admin.site.register(Videos, VideosAdmin)
admin.site.register(Projects, ProjectsAdmin)
admin.site.register(MediaTypes, MediaTypesAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(BaseInfo, BaseInfoAdmin)
admin.site.register(Team, TeamAdmin)
admin.site.register(Competence, CompetenceAdmin)
admin.site.register(Partners, PartnersAdmin)
admin.site.register(Documents, DocumentsAdmin)
admin.site.unregister(Group)
admin.site.unregister(Attachment)
