# Админ панель для SVOE
Сервис написан на Python/Django
## Начало установки
### Stack
- Python 3.8
- Django 3.2.2
Перед началом установки проекта в системе должен быть установлен Python
## Установка
### 1. Перейти в папку проекта
*Это папка с файлом `manage.py`
Все дальшейшие действия совершаются внутри неё*
### 2. Создать и активировать виртуальное окружение
```
python -m venv venv
source venv/bin/activate
```
*venv - путь к папке виртуального окружения
можно оставить просто venv, и тогда папка создастся в текущей*
### 3. Установить зависимости
```
pip install -r requirements.txt
```
### 4. Добавить переменные окружения
```
source .env
```
**ЗАМЕЧАНИЕ:** 
* SECRET_KEY:  django-insecure-ew%mra+jx+a5ziv(up(f^kfkmu#qd+li5oc-da1a-v+pfwxt&f (секретный ключ django)
* DEBUG: Режим дебага
* ALLOWED_HOSTS: Разрешенные хосты
* DB_NAME: имя БД 
* DB_USER: имя пользователя БД
* DB_PASSWORD: пароль пользователя БД
* DB_HOST: хост БД 
* DB_PORT: порт БД 
### 5. Миграции
В этом сервсисе миграции не нужны, обновления будут происходить в других сервисахё
### 6. Стартовать Guvicorn(Python WSGI HTTP Server) из папки проекта
 gunicorn --bind 0.0.0.0:$PORT svoe_admin.wsgi
 
 Конфиг gunicorn лежит в папке svoe_admin/gunicorn.py

